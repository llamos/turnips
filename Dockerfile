FROM gradle:6.3.0-jdk8 AS builder
ADD ./ /workspace
WORKDIR /workspace
RUN ./gradlew --no-daemon shadowJar

FROM openjdk:8-jre-buster

ENV APPLICATION_USER ktor
RUN useradd $APPLICATION_USER

RUN mkdir /app
RUN chown -R $APPLICATION_USER /app

USER $APPLICATION_USER

COPY --from=builder /workspace/build/libs/turnip-tracker.jar /app/turnip-tracker.jar
COPY --from=builder /workspace/docker-entrypoint.sh /app/docker-entrypoint.sh
WORKDIR /app

CMD ["/bin/bash", "docker-entrypoint.sh"]
