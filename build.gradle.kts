import org.jetbrains.kotlin.gradle.plugin.getKotlinPluginVersion
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

val ktorVersion: String by extra
val exposedDaoVersion: String by extra

buildscript {
    repositories {
        jcenter()
    }
}

plugins {
    application
    kotlin("jvm") version "1.3.70"
    id("org.ajoberstar.grgit") version "4.0.2"
    id("com.github.johnrengelman.shadow") version "5.2.0"
}

group = "com.duckblade"
version = "1.0.0"
application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

tasks.withType(ShadowJar::class) {
    manifest {
        attributes["Main-Class"] = application.mainClassName
    }
    baseName = "turnip-tracker"
    classifier = ""
    version = ""
}

sourceSets {
    main {
        withConvention(KotlinSourceSet::class) {
            kotlin.srcDir("src/main/kotlin")
        }
        resources.srcDir("src/main/resources")
    }
    test {
        withConvention(KotlinSourceSet::class) { kotlin.srcDir("src/test/kotlin") }
        resources.srcDir("src/test/resources")
    }
}

repositories {
    mavenLocal()
    jcenter()
    maven("https://kotlin.bintray.com/ktor")
    maven("https://kotlin.bintray.com/kotlin-js-wrappers")
}

dependencies {
    implementation(kotlin("stdlib", getKotlinPluginVersion()))
    implementation("ch.qos.logback:logback-classic:1.2.1")
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-server-core:$ktorVersion")
    implementation("io.ktor:ktor-html-builder:$ktorVersion")
    implementation("io.ktor:ktor-locations:$ktorVersion")
    implementation("io.ktor:ktor-client-apache:$ktorVersion")
    implementation("io.ktor:ktor-client-auth-jvm:$ktorVersion")
    implementation("io.ktor:ktor-client-json-jvm:$ktorVersion")
    implementation("io.ktor:ktor-client-jackson:$ktorVersion")
    implementation("io.ktor:ktor-client-logging-jvm:$ktorVersion")
    implementation("io.ktor:ktor-server-host-common:$ktorVersion")
    implementation("org.jetbrains.exposed:exposed-core:$exposedDaoVersion")
    implementation("org.jetbrains.exposed:exposed-dao:$exposedDaoVersion")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposedDaoVersion")
    implementation("org.jetbrains.exposed:exposed-jodatime:$exposedDaoVersion")
    implementation("mysql:mysql-connector-java:8.0.15")
    implementation("at.favre.lib:bcrypt:0.9.0")
    implementation("com.zaxxer:HikariCP:3.4.2")
    implementation("org.flywaydb:flyway-core:6.3.3")

    testImplementation("io.ktor:ktor-server-tests:$ktorVersion")
}
