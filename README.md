Turnip Tracker
---

This application is a simple single-page tracker of turnip prices,
allowing for the sharing of turnip prices
among a friend group
with history tracking
and eventually predictions.

#### Contributing
Contributing to the project should be relatively straightforward.
You will need:
* An installation of the JDK
* A Discord Developers Application ([make one here](https://discordapp.com/developers/applications))
* A MySQL server instance

Simply create the following file, replacing `PROPERTY` as you go:

`src/main/resources/development.conf`:
```hocon
db {
  url = "jdbc:mysql://MYSQL_HOST/MYSQL_SCHEMA"
  user = "MYSQL_USER"
  pass = "MYSQL_PASS"
}
discord {
  clientId = "DISCORD_CLIENT_ID"
  clientSecret = "DISCORD_CLIENT_SECRET"
}
```
You can alternatively provide each of these as environment variables.
See `src/main/resources/application.conf` for keys. 

Once you have this file, 
the database will be automatically seeded,
and you can run the application with
`./gradlew run`.
