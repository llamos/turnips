package features

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import controllers.auth.AuthLocation
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationFeature
import io.ktor.client.HttpClient
import io.ktor.client.features.UserAgent
import io.ktor.client.features.auth.Auth
import io.ktor.client.features.auth.providers.basic
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.get
import io.ktor.client.request.header
import io.ktor.client.request.post
import io.ktor.features.origin
import io.ktor.http.ContentType
import io.ktor.http.URLBuilder
import io.ktor.http.contentType
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import io.ktor.request.host
import io.ktor.request.port
import io.ktor.util.AttributeKey
import io.ktor.util.createFromCall

private val clientIdAttributeKey: AttributeKey<String> = AttributeKey("DiscordAuth_ClientId")
private val oauthClientAttributeKey: AttributeKey<HttpClient> = AttributeKey("DiscordAuth_OAuthClient")
private val authenticatedClientAttributeKey: AttributeKey<HttpClient> = AttributeKey("DiscordAuth_Client")
private val installedKey: AttributeKey<Boolean> = AttributeKey("DiscordAuth_Installed")

class DiscordAuth(config: Configuration) {

    val clientId = config.clientId
    val clientSecret = config.clientSecret

    val client = HttpClient() {
        install(JsonFeature)
        install(UserAgent) { agent = "web:com.redditball:v2.0.0 (by /u/llamositopia)" }
    }

    val oauthClient = HttpClient() {
        install(JsonFeature)
        install(UserAgent) { agent = "web:com.redditball:v2.0.0 (by /u/llamositopia)" }
        install(Auth) {
            basic {
                username = clientId
                password = clientSecret
            }
        }
    }

    class Configuration {
        lateinit var clientId: String
        lateinit var clientSecret: String
    }

    companion object Feature : ApplicationFeature<Application, Configuration, DiscordAuth> {
        override val key: AttributeKey<DiscordAuth> = AttributeKey("DiscordAuth")
        override fun install(pipeline: Application, configure: Configuration.() -> Unit): DiscordAuth {
            val config = Configuration().apply(configure)
            val feature = DiscordAuth(config)

            pipeline.attributes.put(clientIdAttributeKey, feature.clientId)
            pipeline.attributes.put(oauthClientAttributeKey, feature.oauthClient)
            pipeline.attributes.put(authenticatedClientAttributeKey, feature.client)
            pipeline.attributes.put(installedKey, true)

            return feature
        }
    }

}

@JsonIgnoreProperties(ignoreUnknown = true)
data class DiscordOAuthToken(
    @JsonProperty("access_token") val accessToken: String
)

@JsonIgnoreProperties(ignoreUnknown = true)
data class DiscordUserInfo(
    val id: String,
    val username: String,
    val discriminator: String
)

object DiscordAuthService {

    @KtorExperimentalLocationsAPI
    fun encodeDiscordRedirectUri(call: ApplicationCall): String {
        val protocol = call.request.origin.scheme
        val host = call.request.host()
        val port = if (call.request.port() != (if (protocol == "http") 80 else 443)) ":${call.request.port()}" else ""
        val callback = call.locations.href(AuthLocation.CallbackLocation.Discord())
        return "${protocol}://$host$port$callback"
    }

    @KtorExperimentalLocationsAPI
    fun buildDiscordOAuthConsentURL(call: ApplicationCall): String {
        return URLBuilder.createFromCall(call).apply {
            val clientId = call.application.attributes[clientIdAttributeKey]

            host = "discordapp.com"
            port = 443
            encodedPath = "/api/oauth2/authorize"
            parameters.clear()
            parameters.append("client_id", clientId)
            parameters.append("response_type", "code")
            parameters.append("state", StateManager.generateState())
            parameters.append("redirect_uri", encodeDiscordRedirectUri(call))
            parameters.append("scope", "identify")
        }.buildString()
    }

    @KtorExperimentalLocationsAPI
    suspend fun getOAuthTokens(authCode: String, call: ApplicationCall): DiscordOAuthToken {
        val client = call.application.attributes[oauthClientAttributeKey]
        return client.post("https://discordapp.com/api/oauth2/token") {
            body = "code=$authCode&redirect_uri=${encodeDiscordRedirectUri(call)}&grant_type=authorization_code"
            contentType(ContentType.Application.FormUrlEncoded)
        }
    }

    suspend fun getIdentity(accessToken: DiscordOAuthToken, call: ApplicationCall): DiscordUserInfo {
        val client = call.application.attributes[authenticatedClientAttributeKey]
        return client.get("https://discordapp.com/api/v6/users/@me") {
            header("Authorization", "Bearer ${accessToken.accessToken}")
        }
    }

    suspend fun revokeToken(accessToken: DiscordOAuthToken, call: ApplicationCall) {
        val client = call.application.attributes[authenticatedClientAttributeKey]
        client.post<String>("https://discordapp.com/api/oauth2/token/revoke") {
            body = "token=${accessToken.accessToken}"
            contentType(ContentType.Application.FormUrlEncoded)
        }
    }

}
