package features

import kotlin.math.pow
import kotlin.math.roundToInt

object StateManager {

    private val states = mutableMapOf<String, Long>()

    fun generateState(): String {
        val state = (Math.random() * 36F.pow(6F)).roundToInt().toString(36)
        states[state] = System.currentTimeMillis()
        return state
    }

    fun confirmState(state: String): Boolean {
        val timestamp = states[state] ?: return false
        return timestamp > System.currentTimeMillis() - (5 * 60 * 1000)
    }

}
