package features

import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.ApplicationFeature
import io.ktor.application.install
import io.ktor.sessions.*
import io.ktor.util.AttributeKey
import io.ktor.util.KtorExperimentalAPI
import model.User
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

class UnauthenticatedException : Exception()
class UnauthorizedException : Exception()

@Suppress("unused_parameter")
class TurnipAuth(config: Configuration) {

    class Configuration {}

    @KtorExperimentalAPI
    companion object Feature : ApplicationFeature<Application, Configuration, TurnipAuth> {
        override val key: AttributeKey<TurnipAuth> = AttributeKey("TurnipAuth")
        override fun install(pipeline: Application, configure: Configuration.() -> Unit): TurnipAuth {
            val config = Configuration().apply(configure)
            val feature = TurnipAuth(config)

            val privateKey = pipeline.environment.config.property("ktor.session.secret").getString()

            pipeline.install(Sessions) {
                cookie<RBAuthCookie>("TurnipAuth") {
                    cookie.path = "/"
                    cookie.maxAgeInSeconds = 365L * 24L * 60L * 60L
                    transform(SessionTransportTransformerMessageAuthentication(privateKey.toByteArray()))
                }
            }

            return feature
        }
    }

    data class RBAuthCookie(val id: Int)

}

private val UserAttributeKey = AttributeKey<User>("TurnipAuth_User")
suspend fun ApplicationCall.getUser(): User? {
    val stored = attributes.getOrNull(UserAttributeKey)
    if (stored != null)
        return stored

    val cookie = sessions.get<TurnipAuth.RBAuthCookie>()
        ?: return null
    val uId = cookie.id

    val user = newSuspendedTransaction { User.findById(uId) }
        ?: return null

    attributes.put(UserAttributeKey, user)
    return user
}
