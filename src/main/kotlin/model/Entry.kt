package model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.date
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

enum class EntryStamp(val displayName: String) {
    SUNDAY("Sun (Buy)"),
    MON_AM("Mon AM"),
    MON_PM("Mon PM"),
    TUE_AM("Tue AM"),
    TUE_PM("Tue PM"),
    WED_AM("Wed AM"),
    WED_PM("Wed PM"),
    THU_AM("Thu AM"),
    THU_PM("Thu PM"),
    FRI_AM("Fri AM"),
    FRI_PM("Fri PM"),
    SAT_AM("Sat AM"),
    SAT_PM("Sat PM");
    val day get() = valueOf(this.name.replace("PM", "AM"))
    companion object {
        fun dayValues(): Array<EntryStamp> = arrayOf(SUNDAY, MON_AM, TUE_AM, WED_AM, THU_AM, FRI_AM, SAT_AM)
    }
}

object Entries : IntIdTable(name = "entries") {
    val week = datetime(name = "week")
    val entryStamp = datetime(name = "entry_stamp").clientDefault { DateTime.now() }
    val entryStampEnum = enumeration(name = "entry_stamp_enum", klass = EntryStamp::class)
    val island = reference(name = "island", foreign = Islands, fkName = "fk_entries_island")
    val price = integer(name = "price")
}

class Entry(id: EntityID<Int>) : IntEntity(id), Comparable<Entry> {
    companion object : IntEntityClass<Entry>(Entries)

    var week by Entries.week.transform({ it }, { it.withZoneRetainFields(DateTimeZone.getDefault()) })
    var entryStamp by Entries.entryStamp
    var entryStampEnum by Entries.entryStampEnum
    var island by Island referencedOn Entries.island
    var price by Entries.price

    override fun compareTo(other: Entry): Int = when {
        week.compareTo(other.week) != 0 -> week.compareTo(other.week)
        entryStamp.compareTo(other.entryStamp) != 0 -> entryStamp.compareTo(other.entryStamp)
        else -> id.compareTo(other.id)
    }

}
