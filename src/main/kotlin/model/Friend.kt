package model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object Friends : IntIdTable(name = "friends") {

    val owner = reference(name = "owner", foreign = Users, fkName = "fk_friends_owner")
    val target = reference(name = "target", foreign = Users, fkName = "fk_friends_target")

}

class Friend(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Friend>(Friends)

    var ownerId by Friends.owner
    var owner by User referencedOn Friends.owner
    var targetId by Friends.target
    var target by User referencedOn Friends.target

}
