package model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime

enum class NPC(val displayName: String) {
    CELESTE("Celeste"),
    FLICK("Flick"),
    SAHARAH("Saharah"),
    LABEL("Label"),
    KICKS("Kicks"),
    CJ("C.J."),
    LEIF("Leif")
}

object NPCVisits : IntIdTable(name = "npc_visits") {

    val island = reference(name = "island", foreign = Islands, fkName = "fk_npc_visits_island")
    val week = datetime(name = "week")
    val entryStampEnum = enumeration(name = "entry_stamp_enum", klass = EntryStamp::class)
    val npc = enumeration(name = "npc", klass = NPC::class)

}

class NPCVisit(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<NPCVisit>(NPCVisits)

    var island by Island referencedOn NPCVisits.island
    var week by NPCVisits.week
    var entryStampEnum by NPCVisits.entryStampEnum
    var npc by NPCVisits.npc

}
