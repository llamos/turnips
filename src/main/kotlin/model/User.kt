package model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Users : IntIdTable(name = "users") {
    val snowflake = varchar(name = "discord_snowflake", length = 18)
    val name = varchar(name = "name", length = 40)
}

class User(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<User>(Users)

    var name by Users.name
    var snowflake by Users.snowflake

    val islands by Island optionalReferrersOn Islands.owner
    val outgoingFriendRequests by FriendRequest referrersOn FriendRequests.sender
    val incomingFriendRequests by FriendRequest referrersOn FriendRequests.receiver
    val friends by Friend referrersOn Friends.owner
}
