package model

import controllers.islands.IslandsLocation
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import model.Island.Companion.transform
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

object Islands : IntIdTable(name = "islands") {
    val name = varchar(name = "island_name", length = 20)
    val passwordHash = text(name = "password_hash").nullable()
    val owner = optReference(name = "owner", foreign = Users, fkName = "fk_islands_owner")
    val lastEntryStamp = datetime(name = "last_entry_stamp").nullable()
    val timezone = varchar(name = "timezone", length = 60)
}

class Island(id: EntityID<Int>) : IntEntity(id), Comparable<Island> {
    companion object : IntEntityClass<Island>(Islands)

    var name by Islands.name
    var passwordHash by Islands.passwordHash
    var ownerId by Islands.owner
    var owner by User optionalReferencedOn Islands.owner
    var lastEntryStamp by Islands.lastEntryStamp
    var timezone by Islands.timezone.transform({ it.id }, { DateTimeZone.forID(it) })

    val startOfWeek get() = DateTime.now(timezone).withDayOfWeek(1).minusDays(1).withTimeAtStartOfDay()
    val lastEntry by lazy { entries.max() }
    val entries by Entry referrersOn Entries.island
    val npcVisits by NPCVisit referrersOn NPCVisits.island

    @KtorExperimentalLocationsAPI
    fun href(call: ApplicationCall) = call.locations.href(IslandsLocation.Identified(id.value))

    override fun compareTo(other: Island): Int = when {
        lastEntry == null && other.lastEntry == null -> id.compareTo(other.id)
        lastEntry == null -> -1
        other.lastEntry == null -> 1
        else -> lastEntry!!.compareTo(other.lastEntry!!)
    }
}
