package model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.jodatime.datetime
import org.joda.time.DateTime

object FriendRequests : IntIdTable(name = "friend_requests") {

    val sender = reference(name = "sender", foreign = Users, fkName = "fk_friend_requests_sender")
    val receiver = reference(name = "receiver", foreign = Users, fkName = "fk_friend_requests_receiver")
    val entryStamp = datetime(name = "entry_stamp").clientDefault { DateTime.now() }

}

class FriendRequest(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<FriendRequest>(FriendRequests)

    var sender by User referencedOn FriendRequests.sender
    var receiver by User referencedOn FriendRequests.receiver
    var entryStamp by FriendRequests.entryStamp

}
