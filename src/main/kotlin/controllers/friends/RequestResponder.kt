package controllers.friends

import controllers.Responder
import controllers.islands.IslandsLocation
import features.UnauthenticatedException
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect
import model.*
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.or
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import views.friends.respondFriendPage

@KtorExperimentalLocationsAPI
object RequestResponder : Responder<FriendsLocation.Requests> {

    // just in case someone gets stuck on the form submission url
    override suspend fun ApplicationCall.get(location: FriendsLocation.Requests) {
        respondRedirect(locations.href(FriendsLocation()))
    }

    override suspend fun ApplicationCall.post(location: FriendsLocation.Requests) {
        newSuspendedTransaction {
            val params = receiveParameters()
            val action = params["action"] ?: throw UnsupportedOperationException()
            val snowflake = params["snowflake"] ?: throw UnsupportedOperationException()

            val islandRedirect = params["redirect-island"]?.toInt()?.let { islandId ->
                suspend { respondRedirect(locations.href(IslandsLocation.Identified(islandId))) }
            }

            // get the current and target user
            val currentUser = getUser() ?: throw UnauthenticatedException()
            val targetUser = User.find { Users.snowflake eq snowflake }.singleOrNull()
                ?: return@newSuspendedTransaction islandRedirect?.invoke() ?: respondFriendPage("Invalid Discord snowflake")

            // check if they're already friends (can't handle here)
            val existingFriendship = Friend.find { (Friends.owner eq currentUser.id) and (Friends.target eq targetUser.id) }
            if (!existingFriendship.empty())
                return@newSuspendedTransaction respondFriendPage(error = "You are already friends with this user.")

            when (action) {
                "add" -> handleAdd(currentUser, targetUser, islandRedirect)
                "remove" -> handleRemove(currentUser, targetUser, islandRedirect)
                else -> throw UnsupportedOperationException()
            }
        }
    }

    private suspend fun ApplicationCall.handleAdd(currentUser: User, targetUser: User, islandRedirect: (suspend () -> Unit)?) {

        // make sure we don't add our self
        if (currentUser.id.value == targetUser.id.value)
            return islandRedirect?.invoke() ?: respondFriendPage("You can't add yourself as a friend.")

        // check if we've already sent a request to this user
        val existingRequest = FriendRequest.find { (FriendRequests.sender eq currentUser.id) and (FriendRequests.receiver eq targetUser.id) }
        if (!existingRequest.empty())
            return islandRedirect?.invoke() ?: respondFriendPage()

        // check for an existing friend req from the target user
        val existingPending = FriendRequest.find { (FriendRequests.sender eq targetUser.id) and (FriendRequests.receiver eq currentUser.id) }.firstOrNull()
        if (existingPending != null) {
            // accept it
            Friend.new {
                owner = currentUser
                target = targetUser
            }
            Friend.new {
                owner = targetUser
                target = currentUser
            }
            existingPending.delete()
            return islandRedirect?.invoke() ?: respondFriendPage()
        }

        // make a new req
        FriendRequest.new {
            sender = currentUser
            receiver = targetUser
        }
        islandRedirect?.invoke() ?: respondFriendPage()
    }

    private suspend fun ApplicationCall.handleRemove(currentUser: User, targetUser: User, islandRedirect: (suspend () -> Unit)?) {
        // delete existing requests with the target user
        FriendRequest.find {
            ((FriendRequests.sender eq currentUser.id) and (FriendRequests.receiver eq targetUser.id)) or
                    ((FriendRequests.sender eq targetUser.id) and (FriendRequests.receiver eq currentUser.id))
        }.forEach { it.delete() }
        islandRedirect?.invoke() ?: respondFriendPage()
    }

}
