package controllers.friends

import controllers.Controller
import controllers.friends.FriendsResponder.get
import controllers.friends.FriendsResponder.post
import controllers.friends.RequestResponder.get
import controllers.friends.RequestResponder.post
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.routing.Route

@KtorExperimentalLocationsAPI
@Location(path = "friends")
class FriendsLocation {
    @Location(path = "requests")
    data class Requests(val parent: FriendsLocation = FriendsLocation())
}

@KtorExperimentalLocationsAPI
object FriendsController : Controller {

    override fun Route.routeImpl() {
        get<FriendsLocation> { call.get(it) }
        post<FriendsLocation> { call.post(it) }

        get<FriendsLocation.Requests> { call.get(it) }
        post<FriendsLocation.Requests> { call.post(it) }
    }

}
