package controllers.friends

import controllers.Responder
import features.UnauthenticatedException
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveParameters
import model.Friend
import model.Friends
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import views.friends.respondFriendPage

@KtorExperimentalLocationsAPI
object FriendsResponder : Responder<FriendsLocation> {

    override suspend fun ApplicationCall.get(location: FriendsLocation) {
        newSuspendedTransaction {
            respondFriendPage()
        }
    }

    override suspend fun ApplicationCall.post(location: FriendsLocation) {
        newSuspendedTransaction {
            val params = receiveParameters()
            val action = params["action"] ?: throw UnsupportedOperationException()
            val friendId = params["friend"]?.toInt() ?: throw UnsupportedOperationException()

            // get the target friendship object (fail silently if not found)
            val currentUser = getUser() ?: throw UnauthenticatedException()
            val friendship = Friend.find {
                (Friends.owner eq currentUser.id) and (Friends.id eq friendId)
            }.singleOrNull()
                ?: return@newSuspendedTransaction respondFriendPage()

            // find the corresponding friendship for the other user too
            val backFriendship = Friend.find {
                (Friends.target eq currentUser.id) and (Friends.owner eq friendship.targetId)
            }.singleOrNull()

            when (action) {
                "remove" -> {
                    friendship.delete()
                    backFriendship?.delete()
                }
                else -> throw UnsupportedOperationException()
            }
            respondFriendPage()
        }
    }

}
