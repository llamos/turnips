package controllers.index

import controllers.Responder
import features.UnauthenticatedException
import features.UnauthorizedException
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect
import model.Entry
import model.EntryStamp
import model.Island
import model.Islands
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.joda.time.DateTime
import views.index.respondIndexPage

@KtorExperimentalLocationsAPI
object IndexResponder : Responder<IndexLocation> {
    private fun entriesThisWeek(): List<Island> {
        return Island.find { Islands.lastEntryStamp greaterEq DateTime.now().minusWeeks(1) }
            .orderBy(Islands.lastEntryStamp to SortOrder.DESC)
            .toList()
    }

    override suspend fun ApplicationCall.get(location: IndexLocation) {
        newSuspendedTransaction {
            respondIndexPage(entriesThisWeek())
        }
    }

    override suspend fun ApplicationCall.post(location: IndexLocation) {
        newSuspendedTransaction {
            val params = receiveParameters()

            // check params
            val entryIsland = params["island"]?.toIntOrNull()
            val entryPrice = params["price"]?.toIntOrNull()
            val entryStamp = params["entryStamp"]?.toIntOrNull()?.let { EntryStamp.values().getOrNull(it) }
            if (entryIsland == null)
                return@newSuspendedTransaction respondIndexPage(entriesThisWeek(), error = "You must select an island.", pricePrefill = entryPrice, entryStampPrefill = entryStamp?.ordinal)
            if (entryPrice == null)
                return@newSuspendedTransaction respondIndexPage(entriesThisWeek(), error = "You must provide the turnip price.", islandPrefill = entryIsland, entryStampPrefill = entryStamp?.ordinal)
            if (entryStamp == null)
                return@newSuspendedTransaction respondIndexPage(entriesThisWeek(), error = "You must provide the time.", islandPrefill = entryIsland, pricePrefill = entryPrice)

            // check logged in
            val user = getUser()
                ?: throw UnauthenticatedException()

            // pull island from db
            val targetIsland = Island.findById(entryIsland)
                ?: throw UnsupportedOperationException()

            // check user has island perms
            if (targetIsland.ownerId != user.id)
                throw UnauthorizedException()

            Entry.new {
                this.island = targetIsland
                this.price = entryPrice
                this.entryStampEnum = entryStamp
                this.week = targetIsland.startOfWeek
            }

            targetIsland.lastEntryStamp = DateTime.now()
        }

        respondRedirect(url = locations.href(IndexLocation()), permanent = false)
    }
}
