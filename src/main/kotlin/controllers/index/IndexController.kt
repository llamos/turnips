package controllers.index

import controllers.Controller
import controllers.index.IndexResponder.get
import controllers.index.IndexResponder.post
import controllers.index.NPCVisitResponder.post
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.routing.Route

@KtorExperimentalLocationsAPI
@Location(path = "")
class IndexLocation {
    @Location(path = "visit")
    data class NPCVisit(val parent: IndexLocation = IndexLocation())
}

object IndexController : Controller {

    @KtorExperimentalLocationsAPI
    override fun Route.routeImpl() {
        get<IndexLocation> { call.get(it) }
        post<IndexLocation> { call.post(it) }

        post<IndexLocation.NPCVisit> { call.post(it) }
    }

}
