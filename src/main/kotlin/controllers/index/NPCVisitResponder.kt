package controllers.index

import controllers.Responder
import features.UnauthenticatedException
import features.UnauthorizedException
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect
import model.EntryStamp
import model.Island
import model.NPC
import model.NPCVisit
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

@KtorExperimentalLocationsAPI
object NPCVisitResponder : Responder<IndexLocation.NPCVisit> {

    override suspend fun ApplicationCall.post(location: IndexLocation.NPCVisit) {
        newSuspendedTransaction {
            val user = getUser() ?: throw UnauthenticatedException()
            val params = receiveParameters()

            val reqIsland = params["island"]?.toIntOrNull()?.let { Island.findById(it) } ?: throw UnsupportedOperationException()
            val stamp = params["entryStamp"]?.toIntOrNull()?.let { EntryStamp.values().getOrNull(it) } ?: throw UnsupportedOperationException()
            val reqVisitor = params["visitor"]?.toIntOrNull()?.let { NPC.values().getOrNull(it) } ?: throw UnsupportedOperationException()

            if (reqIsland.ownerId != user.id) throw UnauthorizedException()

            NPCVisit.new {
                island = reqIsland
                week = reqIsland.startOfWeek
                entryStampEnum = stamp
                npc = reqVisitor
            }
        }

        respondRedirect(locations.href(IndexLocation()))
    }

}
