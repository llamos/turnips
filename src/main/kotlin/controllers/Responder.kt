package controllers

import io.ktor.application.ApplicationCall

interface Responder<T> {
    suspend fun ApplicationCall.get(location: T) { throw UnsupportedOperationException() }
    suspend fun ApplicationCall.post(location: T) { throw UnsupportedOperationException() }
    suspend fun ApplicationCall.patch(location: T) { throw UnsupportedOperationException() }
    suspend fun ApplicationCall.put(location: T) { throw UnsupportedOperationException() }
    suspend fun ApplicationCall.delete(location: T) { throw UnsupportedOperationException() }
}
