package controllers.error

import features.UnauthenticatedException
import features.UnauthorizedException
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.pipeline.PipelineContext
import views.error.*

typealias ErrorResponder<T> = suspend PipelineContext<Unit, ApplicationCall>.(T) -> Unit

@KtorExperimentalLocationsAPI
val UnsupportedOperationResponder: ErrorResponder<UnsupportedOperationException> = {
    call.respondNotFound()
}

@KtorExperimentalLocationsAPI
val UnauthenticatedResponder: ErrorResponder<UnauthenticatedException> = {
    call.respondUnauthenticated()
}

@KtorExperimentalLocationsAPI
val UnauthorizedResponder: ErrorResponder<UnauthorizedException> = {
    call.respondUnauthorized()
}

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
val ExceptionResponder: ErrorResponder<Exception> = { e ->
    e.printStackTrace()
    call.respondInternalServerError(e)
}
