package controllers

import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.routing.*

interface Controller {

    @KtorExperimentalLocationsAPI
    fun route(routing: Route) { routing.routeImpl() }

    @KtorExperimentalLocationsAPI
    fun Route.routeImpl()
}
