package controllers.islands

import controllers.Responder
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import model.Island
import model.Islands
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.joda.time.DateTime
import views.islands.respondIslandListPage

@KtorExperimentalLocationsAPI
object ListResponder : Responder<IslandsLocation> {
    override suspend fun ApplicationCall.get(location: IslandsLocation) {
        newSuspendedTransaction {
            val islands = Island.find { Islands.lastEntryStamp greaterEq DateTime.now().minusDays(7) }

            respondIslandListPage(islands)
        }
    }

}
