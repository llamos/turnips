package controllers.islands

import views.islands.respondIslandPage
import controllers.Responder
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import model.Island
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

@KtorExperimentalLocationsAPI
object IslandResponder : Responder<IslandsLocation.Identified> {
    override suspend fun ApplicationCall.get(location: IslandsLocation.Identified) {
        newSuspendedTransaction {
            // get island name from route params
            val island = Island.findById(location.id)
                ?: throw UnsupportedOperationException()

            respondIslandPage(island)
        }
    }
}
