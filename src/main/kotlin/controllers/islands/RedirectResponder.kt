package controllers.islands

import controllers.Responder
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respondRedirect
import model.Entries
import model.Entry
import model.EntryStamp
import model.Island
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.joda.time.DateTime

@KtorExperimentalLocationsAPI
object RedirectResponder : Responder<IslandsLocation.Identified.Redirect> {
    override suspend fun ApplicationCall.get(location: IslandsLocation.Identified.Redirect) {
        newSuspendedTransaction {
            // get island name from route params
            val island = Island.findById(location.parent.id)
                ?: throw UnsupportedOperationException()

            // get entries by island name
            val now = DateTime.now(island.timezone).withDayOfWeek(1).minusDays(1)
            val entries =
                Entry.find { (Entries.week eq now) and (Entries.island eq island.id) }
                    .orderBy(Entries.entryStamp to SortOrder.ASC)
                    .toList()

            // map entries to their spot in the list
            val outs = mutableListOf<Int>()
            val times = EntryStamp.values()
            for (i in times.indices) {
                val entry =
                    entries.lastOrNull { it.entryStampEnum == times[i] }
                outs.add(entry?.price ?: 0)
            }

            respondRedirect("https://turnipprophet.io/index.html?prices=${outs.joinToString(".")}")
        }
    }

}
