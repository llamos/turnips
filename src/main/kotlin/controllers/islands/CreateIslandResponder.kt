package controllers.islands

import controllers.Responder
import features.UnauthenticatedException
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect
import model.Island
import model.Islands
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.joda.time.DateTimeZone
import views.islands.respondIslandCreatePage

@KtorExperimentalLocationsAPI
object CreateIslandResponder : Responder<IslandsLocation.New> {

    override suspend fun ApplicationCall.get(location: IslandsLocation.New) {
        newSuspendedTransaction {
            if (getUser() == null)
                throw UnauthenticatedException()

            respondIslandCreatePage()
        }
    }

    override suspend fun ApplicationCall.post(location: IslandsLocation.New) {
        newSuspendedTransaction {
            val user = getUser()
                ?: throw UnauthenticatedException()

            val params = receiveParameters()
            val islandName = params["name"] ?: throw UnsupportedOperationException()
            val reqTimezone = params["timezone"]?.let { DateTimeZone.forID(it) } ?: throw UnsupportedOperationException()

            val existing = !(Island.find { (Islands.owner eq user.id) and (Islands.name eq islandName) }.empty())
            if (existing)
                return@newSuspendedTransaction respondIslandCreatePage("You already have an island named $islandName")

            val newIsland = Island.new {
                owner = user
                name = islandName
                timezone = reqTimezone
            }

            respondRedirect(newIsland.href(this@post))
        }
    }

}
