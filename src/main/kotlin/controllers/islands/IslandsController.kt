package controllers.islands

import controllers.Controller
import controllers.islands.IslandResponder.get
import controllers.islands.ListResponder.get
import controllers.islands.RedirectResponder.get
import controllers.islands.ClaimResponder.get
import controllers.islands.ClaimResponder.post
import controllers.islands.CreateIslandResponder.get
import controllers.islands.CreateIslandResponder.post
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.locations.post
import io.ktor.routing.Route
import org.joda.time.DateTime
import org.joda.time.DateTimeZone

@KtorExperimentalLocationsAPI
@Location(path = "islands")
class IslandsLocation {
    @Location(path = "{id}")
    data class Identified(val id: Int, val parent: IslandsLocation = IslandsLocation()) {
        @Location(path = "redirect")
        data class Redirect(val parent: Identified) {
            constructor(id: Int) : this(Identified(id))
        }
        @Location(path = "claim")
        data class Claim(val parent: Identified) {
            constructor(id: Int) : this(Identified(id))
        }
    }

    @Location(path = "new")
    data class New(val parent: IslandsLocation = IslandsLocation())
}

object IslandsController : Controller {

    @KtorExperimentalLocationsAPI
    override fun Route.routeImpl() {
        get<IslandsLocation> { call.get(it) }
        get<IslandsLocation.Identified> { call.get(it) }
        get<IslandsLocation.Identified.Redirect> { call.get(it) }

        get<IslandsLocation.Identified.Claim> { call.get(it) }
        post<IslandsLocation.Identified.Claim> { call.post(it) }

        get<IslandsLocation.New> { call.get(it) }
        post<IslandsLocation.New> { call.post(it) }
    }

}
