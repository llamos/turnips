package controllers.islands

import at.favre.lib.crypto.bcrypt.BCrypt
import controllers.Responder
import features.UnauthenticatedException
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.receiveParameters
import io.ktor.response.respondRedirect
import model.Island
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import views.islands.respondIslandClaimPage

@KtorExperimentalLocationsAPI
object ClaimResponder : Responder<IslandsLocation.Identified.Claim> {

    override suspend fun ApplicationCall.get(location: IslandsLocation.Identified.Claim) {
        newSuspendedTransaction {
            // find island
            val requestedIsland = Island.findById(location.parent.id)
                ?: throw UnsupportedOperationException()

            // check logged in
            if (getUser() == null)
                throw UnauthenticatedException()

            if (requestedIsland.owner != null)
                throw UnsupportedOperationException()

            respondIslandClaimPage(requestedIsland)
        }
    }

    override suspend fun ApplicationCall.post(location: IslandsLocation.Identified.Claim) {
        newSuspendedTransaction {
            // find island
            val requestedIsland = Island.findById(location.parent.id)
                ?: throw UnsupportedOperationException()

            // check logged in
            val user = getUser() ?: throw UnauthenticatedException()

            // check pw
            val params = receiveParameters()
            val hash = requestedIsland.passwordHash
            if (hash != null) {
                val providedPass = params["password"]
                if (providedPass.isNullOrBlank())
                    return@newSuspendedTransaction respondIslandClaimPage(requestedIsland, true)

                if (!BCrypt.verifyer().verify(providedPass.toCharArray(), hash).verified)
                    return@newSuspendedTransaction respondIslandClaimPage(requestedIsland, true)
            }

            // do claim
            requestedIsland.owner = user
            respondRedirect(requestedIsland.href(this@post))
        }
    }

}
