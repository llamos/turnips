package controllers.auth

import controllers.Responder
import controllers.index.IndexLocation
import features.DiscordAuthService
import features.TurnipAuth
import features.StateManager
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import io.ktor.response.respondRedirect
import io.ktor.sessions.sessions
import io.ktor.sessions.set
import model.User
import model.Users
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

@KtorExperimentalLocationsAPI
object DiscordCallbackResponder : Responder<AuthLocation.CallbackLocation.Discord> {

    override suspend fun ApplicationCall.get(location: AuthLocation.CallbackLocation.Discord) {
        val state = location.state ?: throw UnsupportedOperationException()
        val code = location.code ?: throw UnsupportedOperationException()

        if (!StateManager.confirmState(state))
            throw UnsupportedOperationException()

        val tokens = DiscordAuthService.getOAuthTokens(code, this)
        val identity = DiscordAuthService.getIdentity(tokens, this)
//        DiscordAuthService.revokeToken(tokens, this)

        // logging in with discord
        newSuspendedTransaction {
            val user = User.find { Users.snowflake eq identity.id }.singleOrNull()
                ?: User.new {
                    snowflake = identity.id
                    name = "${identity.username}#${identity.discriminator}"
                }

            user.name = "${identity.username}#${identity.discriminator}"

            sessions.set(TurnipAuth.RBAuthCookie(user.id.value))
            respondRedirect(locations.href(IndexLocation()))
        }
    }

}
