package controllers.auth

import controllers.Controller
import controllers.auth.RedirectToDiscordResponder.get
import controllers.auth.DiscordCallbackResponder.get
import features.TurnipAuth
import io.ktor.application.call
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location
import io.ktor.locations.get
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.sessions.clear
import io.ktor.sessions.sessions

@KtorExperimentalLocationsAPI
@Location(path = "auth")
class AuthLocation {
    @Location(path = "signin")
    data class SignInLocation(val parent: AuthLocation = AuthLocation()) {
        @Location(path = "discord")
        data class Discord(val parent: SignInLocation = SignInLocation())
    }

    @Location(path = "callback")
    data class CallbackLocation(val parent: AuthLocation = AuthLocation()) {
        @Location(path = "discord")
        data class Discord(val state: String? = null, val code: String? = null, val parent: CallbackLocation = CallbackLocation())
    }

    @Location(path = "signout")
    class SignOutLocation(val parent: AuthLocation = AuthLocation())
}

@KtorExperimentalLocationsAPI
object AuthController : Controller {

    override fun Route.routeImpl() {
        get<AuthLocation.SignOutLocation> {
            call.sessions.clear<TurnipAuth.RBAuthCookie>()
            call.respondRedirect(url = "/")
        }

        get<AuthLocation.SignInLocation.Discord> { call.get(it) }
        get<AuthLocation.CallbackLocation.Discord> { call.get(it) }
    }
}
