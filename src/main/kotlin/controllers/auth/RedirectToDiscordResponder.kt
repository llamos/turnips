package controllers.auth

import controllers.Responder
import features.DiscordAuthService
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.response.respondRedirect

@KtorExperimentalLocationsAPI
object RedirectToDiscordResponder : Responder<AuthLocation.SignInLocation.Discord> {

    override suspend fun ApplicationCall.get(location: AuthLocation.SignInLocation.Discord) {
        respondRedirect(
            DiscordAuthService.buildDiscordOAuthConsentURL(this@get)
        )
    }

}
