package views.util

import kotlinx.html.*
import kotlinx.html.attributes.EnumEncoder
import kotlinx.html.attributes.enumEncode

@Suppress("unused")
open class INPUTGROUP(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) :
    DIV(initialAttributes, consumer) {}

@Suppress("unused")
open class BOOTSTRAPROW(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) :
    DIV(initialAttributes, consumer) {}

@HtmlTagMarker
fun FlowContent.inputGroup(classes: String? = null, block: INPUTGROUP.() -> Unit = {}): Unit =
    INPUTGROUP(attributesMapOf("class", "input-group ${classes ?: ""}"), consumer).visit(block)

@HtmlTagMarker
fun INPUTGROUP.prepend(classes: String? = null, block: DIV.() -> Unit = {}) {
    div(classes = "input-group-prepend ${classes ?: ""}", block = block)
}

@HtmlTagMarker
fun INPUTGROUP.append(classes: String? = null, block: DIV.() -> Unit = {}) {
    div(classes = "input-group-append ${classes ?: ""}", block = block)
}

@HtmlTagMarker
fun FlowContent.container(classes: String? = null, block: DIV.() -> Unit = {}) {
    div(classes = "container ${classes ?: ""}", block = block)
}

@HtmlTagMarker
fun FlowContent.row(classes: String? = null, block: BOOTSTRAPROW.() -> Unit = {}): Unit =
    BOOTSTRAPROW(attributesMapOf("class", "row ${classes ?: ""}"), consumer).visit(block)

@HtmlTagMarker
fun FlowContent.jumbotron(classes: String? = null, block: DIV.() -> Unit = {}): Unit =
    div(classes = "jumbotron ${classes ?: ""}", block = block)

@Suppress("unused")
enum class ResponsiveBreakpoint(override val realValue: String) : AttributeEnum {
    XS("xs"),
    SM("sm"),
    MD("md"),
    LG("lg"),
    XL("xl")
}

@HtmlTagMarker
fun BOOTSTRAPROW.col(breakpoint: ResponsiveBreakpoint? = null, flexBoxWidth: Int? = null, classes: String? = null, block: DIV.() -> Unit = {}) {
    div(classes = "col${breakpoint?.let { "-${it.enumEncode()}" } ?: ""}${flexBoxWidth?.let { "-${it}" } ?: ""} ${classes ?: ""}", block = block)
}

@Suppress("unused")
enum class BtnType(override val realValue: String) : AttributeEnum {
    PRIMARY("btn-primary"),
    SECONDARY("btn-secondary"),
    SUCCESS("btn-success"),
    DANGER("btn-danger"),
    WARNING("btn-warning"),
    INFO("btn-info"),
    DARK("btn-dark"),
    LIGHT("btn-light"),
    DISABLED("disabled")
}

@HtmlTagMarker
fun FlowOrInteractiveOrPhrasingContent.btn(btnType: BtnType, formEncType : ButtonFormEncType? = null, formMethod : ButtonFormMethod? = null, name : String? = null, type : ButtonType? = null, classes : String? = null, block : BUTTON.() -> Unit = {}) : Unit = BUTTON(attributesMapOf("class", "btn ${btnType.enumEncode()} ${classes ?: ""}", "formenctype", formEncType?.enumEncode(),"formmethod", formMethod?.enumEncode(),"name", name,"type", type?.enumEncode()), consumer).visit(block)

@HtmlTagMarker
fun FlowOrInteractiveOrPhrasingContent.aBtn(btnType: BtnType, href: String? = null, target : String? = null, classes : String? = null, block : A.() -> Unit = {}) : Unit = A(attributesMapOf("class", "btn ${btnType.enumEncode()} ${classes ?: ""}","href", href,"target", target), consumer).visit(block)

fun UL.navItem(href: String, active: Boolean = false, block: A.() -> Unit) {
    li(classes = if (active) "nav-item active" else "nav-item") {
        a(href = href, classes = "nav-link", block = block)
    }
}

@Suppress("unused")
open class DROPDOWNMENU(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) :
    DIV(initialAttributes, consumer) {}

@HtmlTagMarker
fun FlowContent.dropdownMenu(classes: String? = null, block: DROPDOWNMENU.() -> Unit = {}): Unit =
    DROPDOWNMENU(attributesMapOf("class", "dropdown-menu ${classes ?: ""}"), consumer).visit(block)

@HtmlTagMarker
fun DROPDOWNMENU.dropdownItem(href: String? = null, target: String? = null, classes: String? = null, block: A.() -> Unit = {}): Unit =
    a(href = href, target = target, classes = "dropdown-item ${classes ?: ""}", block = block)

@HtmlTagMarker
fun DROPDOWNMENU.separator(classes: String? = null, block: DIV.() -> Unit = {}): Unit =
    div(classes = "dropdown-divider ${classes ?: ""}") {
        role = "separator"
        block()
    }

@Suppress("unused")
open class CARD(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) :
    DIV(initialAttributes, consumer) {}

@Suppress("unused")
open class CARDBODY(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) :
    DIV(initialAttributes, consumer) {}

@HtmlTagMarker
fun FlowContent.card(classes: String? = null, block: CARD.() -> Unit = {}): Unit =
    CARD(attributesMapOf("class", "card ${classes ?: ""}"), consumer).visit(block)

@HtmlTagMarker
fun FlowContent.cardDeck(classes: String? = null, block: DIV.() -> Unit = {}): Unit =
    div(classes = "card-deck ${classes ?: ""}", block = block)

@HtmlTagMarker
fun FlowOrInteractiveOrPhrasingContent.cardImgTop(alt : String? = null, src : String? = null, classes : String? = null, block : IMG.() -> Unit = {}) : Unit
        = IMG(attributesMapOf("class", "card-img-top ${classes ?: ""}", "alt", alt,"src", src), consumer).visit(block)

@HtmlTagMarker
fun CARD.cardBody(classes: String? = null, block: CARDBODY.() -> Unit = {}): Unit =
    CARDBODY(attributesMapOf("class", "card-body ${classes ?: ""}"), consumer).visit(block)

@HtmlTagMarker
fun CARD.cardFooter(classes: String? = null, block: DIV.() -> Unit = {}): Unit =
    div(classes = "card-footer ${classes ?: ""}", block = block)

@HtmlTagMarker
fun CARDBODY.cardTitle(classes: String? = null, block: H5.() -> Unit = {}): Unit {
    h5(classes = "card-title ${classes ?: ""}", block = block)
}

@HtmlTagMarker
fun CARDBODY.cardText(classes: String? = null, block: P.() -> Unit = {}): Unit {
    p(classes = "card-text ${classes ?: ""}", block = block)
}
