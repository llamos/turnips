package views.util

import kotlinx.html.*
import kotlinx.html.attributes.enumEncode

@HtmlTagMarker
fun FlowOrInteractiveOrPhrasingContent.label(classes: String? = null, htmlFor: String? = null, block: LABEL.() -> Unit = {}): Unit =
    LABEL(
        attributesMapOf("class", classes, "for", htmlFor), consumer
    ).visit(block)

@HtmlTagMarker
fun FlowOrInteractiveOrPhrasingContent.textArea(rows: String? = null, cols: String? = null, wrap: TextAreaWrap? = null, classes: String? = null, name: String? = null, block: TEXTAREA.() -> Unit = {}): Unit =
    TEXTAREA(attributesMapOf("name", name, "rows", rows, "cols", cols, "wrap", wrap?.enumEncode(), "class", classes), consumer).visit(
        block
    )

fun FlowContent.inputWithLabel(labelText: String, type: InputType? = null, inputId: String = "${labelText.replace(" ", "-")}-id", inputName: String? = null, prefill: String? = null, block: INPUT.() -> Unit = {}) {
    label(htmlFor = inputId) { +labelText }
    input(type = type, classes = "form-control") {
        id = inputId
        if (inputName != null)
            name = inputName
        if (prefill != null)
            value = prefill
        block()
    }
}

fun FlowContent.inputGroupWithLabel(labelText: String, type: InputType? = null, inputId: String = "${labelText.replace(" ", "-")}-id", inputName: String? = null, prefill: String? = null, readonly: Boolean = false, block: INPUTGROUP.() -> Unit = {}) {
    label(htmlFor = inputId) { +labelText }
    inputGroup {
        input(type = type, classes = "form-control") {
            this.readonly = readonly
            id = inputId
            if (inputName != null)
                name = inputName
            if (prefill != null)
                value = prefill
        }
        block()
    }
}

fun FlowContent.customSelect(options: List<Pair<String?, String?>>, inputId: String? = null, inputName: String? = null, prefill: String? = null) {
    select(classes = "custom-select") {
        if (inputId != null)
            id = inputId
        if (inputName != null)
            name = inputName

        for ((optionValue, optionName) in options) {
            option {
                if (prefill == optionValue)
                    selected = true
                if (optionValue != null)
                    value = optionValue
                if (optionName != null)
                    +optionName
            }
        }
    }
}

fun FlowContent.customSelectWithLabel(labelText: String, options: List<Pair<String?, String?>>, inputId: String = "${labelText.replace(" ", "-")}-id", inputName: String? = null, prefill: String? = null, block: INPUTGROUP.() -> Unit = {}) {
    label(htmlFor = inputId) { +labelText }
    inputGroup {
        customSelect(options, inputId, inputName, prefill)
        block()
    }
}

fun FlowContent.yesNoCustomSelectWithLabel(labelText: String, inputId: String = "${labelText.replace(" ", "-")}-id", inputName: String? = null, prefill: Boolean = true, block: INPUTGROUP.() -> Unit = {}) =
    customSelectWithLabel(labelText, listOf("true" to "Yes", "false" to "No"), inputId, inputName, prefill.toString(), block)
