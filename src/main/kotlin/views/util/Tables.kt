package views.util

import kotlinx.html.*

@HtmlTagMarker
fun FlowContent.divTR(classes: String? = null, breakpoint: ResponsiveBreakpoint? = null, block: DIV.() -> Unit)
        = div(classes = "table-responsive${breakpoint?.let { "-${it}" } ?: ""} ${classes ?: ""}", block = block)

@HtmlTagMarker
fun FlowContent.bTable(striped: Boolean = false, sm: Boolean = false, hover: Boolean = false, classes: String? = null, block: TABLE.() -> Unit)
        = table(classes = "table${if (striped) " table-striped" else ""}${if (sm) " table-sm" else ""}${if (hover) " table-hover" else ""} ${classes ?: ""}", block = block)
@HtmlTagMarker
fun FlowContent.bTableTR(striped: Boolean = false, sm: Boolean = false, hover: Boolean = false, classes: String? = null, block: TABLE.() -> Unit)
        = divTR { table(classes = "table${if (striped) " table-striped" else ""}${if (sm) " table-sm" else ""}${if (hover) " table-hover" else ""} ${classes ?: ""}", block = block) }

@HtmlTagMarker
fun TABLE.bTableHeader(vararg headers: String, dark: Boolean = false, classes: String? = null) = thead(classes = "${if (dark) "thead-dark " else ""}${classes ?: ""}") {
    tr {
        for (header in headers) {
            th(ThScope.col) { +header }
        }
    }
}

@HtmlTagMarker
fun <T> TABLE.makeRows(data: Iterable<T>, transform: TR.(T) -> Unit) {
    tbody {
        for (item in data) {
            tr {
                transform(item)
            }
        }
    }
}
