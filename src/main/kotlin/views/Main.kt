package views

import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import kotlinx.html.*

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondTurnipPage(title: String, statusCode: HttpStatusCode = HttpStatusCode.OK, body: DIV.() -> Unit) {
    val reqUser = getUser()

    respondHtml(statusCode) {
        head {
            // page title
            title { +"TurnipTracker - $title" }

            // all the bootstrap
            meta(charset = "utf-8")
            meta(name = "viewport", content="width=device-width, initial-scale=1, shrink-to-fit=no")
            link(rel = "stylesheet", href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css") {}
            script(src="https://code.jquery.com/jquery-3.4.1.slim.min.js") {}
            script(src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js") {}
            script(src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js") {}
        }
        body(classes = "bg-light") {
            // navbar
            turnipNavbar(reqUser, this@respondTurnipPage)

            // create container for content
            div(classes = "container", block = body)
        }
    }
}
