package views.friends

import controllers.friends.FriendsLocation
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import model.Friend
import model.FriendRequest
import model.User
import org.jetbrains.exposed.dao.with
import views.respondTurnipPage
import views.util.*

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondFriendPage(error: String? = null)
{
    val user = getUser() ?: throw UnsupportedOperationException()
    val friends = user.friends.with(Friend::target, User::islands).toList()
    val incomingReqs = user.incomingFriendRequests.with(FriendRequest::sender).toList()
    val outgoingReqs = user.outgoingFriendRequests.with(FriendRequest::receiver).toList()

    respondTurnipPage("Add Friends") {

        h3 { +"My Friends" }
        small("text-muted") {
            +"My Discord ID: ${user.snowflake} ("
            a(href = "https://support.discordapp.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-") {
                +"What's this?"
            }
            +")"
        }
        hr {}

        // region current friends
        row {
            col {
                if (friends.isEmpty()) {
                    p { +"You haven't added any friends yet." }
                } else {
                    bTableTR(striped = true, sm = true, hover = true) {
                        bTableHeader("ID", "Name", "Islands", "Remove?", dark = true)
                        makeRows(friends) { friend ->
                            td { +friend.target.snowflake }
                            td { +friend.target.name }
                            td { +friend.target.islands.joinToString(", ") { it.name } }
                            td {
                                postForm(action = locations.href(FriendsLocation())) {
                                    hiddenInput(name = "friend") { value = friend.id.value.toString() }
                                    btn(BtnType.DANGER, type = ButtonType.submit, name = "action", classes = "btn-sm") {
                                        value = "remove"
                                        +"✖"
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        // endregion

        // region requests
        row {
            // region incoming
            col(ResponsiveBreakpoint.SM, classes = "pt-4") {
                h3 { +"Incoming Friend Requests" }
                hr {}

                if (incomingReqs.isEmpty()) {
                    +"None right now."
                } else {
                    bTableTR(striped = true, sm = true, hover = true) {
                        bTableHeader("ID", "Name", "Accept?", "Reject?", dark = true)
                        makeRows(incomingReqs) { req ->
                            td { +req.sender.snowflake }
                            td { +req.sender.name }
                            td {
                                postForm(action = locations.href(FriendsLocation.Requests())) {
                                    hiddenInput(name = "snowflake") { value = req.sender.snowflake }
                                    btn(BtnType.SUCCESS, type = ButtonType.submit, name = "action", classes = "btn-sm") {
                                        value = "add"
                                        strong { +"+" }
                                    }
                                }
                            }
                            td {
                                postForm(action = locations.href(FriendsLocation.Requests())) {
                                    hiddenInput(name = "snowflake") { value = req.sender.snowflake }
                                    btn(BtnType.DANGER, type = ButtonType.submit, name = "action", classes = "btn-sm") {
                                        value = "remove"
                                        +"✖"
                                    }
                                }
                            }
                        }
                    }
                }
            }
            // endregion

            // region outgoing
            col(ResponsiveBreakpoint.SM, classes = "pt-4") {
                h3 { +"Outgoing Friend Requests" }
                hr {}

                if (outgoingReqs.isEmpty()) {
                    +"None right now."
                } else {
                    bTableTR(striped = true, sm = true, hover = true) {
                        bTableHeader("ID", "Name", "Revoke?", dark = true)
                        makeRows(outgoingReqs) { req ->
                            td { +req.receiver.snowflake }
                            td { +req.receiver.name }
                            td {
                                postForm(action = locations.href(FriendsLocation.Requests())) {
                                    hiddenInput(name = "snowflake") { value = req.receiver.snowflake }
                                    btn(BtnType.DANGER, type = ButtonType.submit, name = "action", classes = "btn-sm") {
                                        value = "remove"
                                        +"✖"
                                    }
                                }
                            }
                        }
                    }
                }

                postForm(action = locations.href(FriendsLocation.Requests()), classes = "pt-4") {
                    label(htmlFor = "form-friend-add-discord") {
                        +"Manual Add by Discord ID "
                    }
                    inputGroup {
                        input(type = InputType.text, name = "snowflake", classes = "form-control") { id = "form-friend-add-discord" }
                        append {
                            btn(BtnType.SUCCESS, type = ButtonType.submit, name = "action", classes = "btn-sm") {
                                value = "add"
                                strong { +"+" }
                            }
                        }
                    }
                    if (error != null)
                        span("text-danger") { +"$error"; br {} }
                    small("form-text text-muted") {
                        +"You can also add friends from their island page."
                    }
                }
            }
            // endregion
        }
        // endregion

    }
}
