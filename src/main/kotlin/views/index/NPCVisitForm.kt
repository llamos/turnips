package views.index

import controllers.index.IndexLocation
import controllers.islands.IslandsLocation
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import model.EntryStamp
import model.NPC
import model.User
import views.util.*

@KtorExperimentalLocationsAPI
fun DIV.npcVisitForm(error: String? = null, user: User, islandPrefill: String? = null, entryStampPrefill: Int? = null, npcPrefill: Int? = null, call: ApplicationCall) {
    postForm(action = call.locations.href(IndexLocation.NPCVisit())) {
        if (error != null) {
            small(classes = "text-danger") {
                +error
            }
        }

        div(classes = "form-group") {
            customSelectWithLabel("Island", user.islands.map { it.id.value.toString() to it.name }, inputName = "island", prefill = islandPrefill) {
                append {
                    aBtn(BtnType.SUCCESS, classes = "text-light", href = call.locations.href(IslandsLocation.New())) { +"New" }
                }
            }
        }

        div(classes = "form-group") {
            customSelectWithLabel("Day", EntryStamp.values().filter { !it.displayName.contains("PM") }.map { it.ordinal.toString() to it.displayName.replace(" (Buy)", "").replace(" AM", "") }, inputName = "entryStamp", prefill = entryStampPrefill.toString())
        }

        val npcOptions = NPC.values().map { it.ordinal.toString() to it.displayName }
        div(classes = "form-group") {
            customSelectWithLabel("Visitor", inputName = "visitor", options = npcOptions, prefill = npcPrefill.toString()) {
                append {
                    btn(BtnType.SUCCESS, type = ButtonType.submit) { +"Save" }
                }
            }
        }
    }
}
