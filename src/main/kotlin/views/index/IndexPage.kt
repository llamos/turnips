package views.index

import controllers.auth.AuthLocation
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import model.*
import views.respondTurnipPage
import views.util.*

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondIndexPage(islands: List<Island>, error: String? = null, islandPrefill: Int? = null, pricePrefill: Int? = null, entryStampPrefill: Int? = null, npcVisitPrefill: Int? = null) {
    val user = getUser()

    respondTurnipPage("Current Prices") {
        h3(classes = "mt-4") { +"Current Prices" }
        hr {}
        bTableTR {
            bTableHeader("Island", "Owner", "Price", "Last Updated", dark = true)
            makeRows(islands) { island ->
                val lastEntry = island.lastEntry!!
                val npc =
                    island.npcVisits.firstOrNull { it.week.isEqual(island.startOfWeek) && it.entryStampEnum == lastEntry.entryStampEnum.day }?.npc
                td {
                    a(href = island.href(this@respondIndexPage)) {
                        +island.name
                        if (npc != null)
                            img(src = "/static/images/npc/${npc.name.toLowerCase()}.png", alt = "(${npc.displayName})", classes = "ml-2 d-inline-block") {
                            style = "height: 1.5rem"
                        }
                    }
                }
                td { +(island.owner?.name ?: "Unclaimed") }
                td { +lastEntry.price.toString() }
                td { +lastEntry.entryStampEnum.displayName }
            }
        }

        if (user != null) {
            row {
                col(ResponsiveBreakpoint.SM) {
                    h3(classes = "mt-4") { +"Submit a new Turnip Price" }
                    hr {}
                    entryForm(error, user, islandPrefill.toString(), entryStampPrefill, pricePrefill, this@respondIndexPage)
                }
                col(ResponsiveBreakpoint.SM) {
//                    style = "border-left: 1px solid rgba(0,0,0,0.1)"
                    h3(classes = "mt-4") { +"Submit an NPC Visit" }
                    hr {}
                    npcVisitForm(error, user, islandPrefill.toString(), entryStampPrefill, npcVisitPrefill, this@respondIndexPage)
                }
            }
        } else {
            span {
                +"To submit an entry, please "
                a(href = locations.href(AuthLocation.SignInLocation.Discord())) { +"sign in" }
                +" first."
            }
        }
    }
}
