package views.index

import controllers.index.IndexLocation
import controllers.islands.IslandsLocation
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import model.EntryStamp
import model.User
import views.util.*

@KtorExperimentalLocationsAPI
fun DIV.entryForm(error: String? = null, user: User, islandPrefill: String? = null, entryStampPrefill: Int? = null, pricePrefill: Int? = null, call: ApplicationCall) {
    postForm(action = call.locations.href(IndexLocation())) {
        if (error != null) {
            small(classes = "text-danger") {
                +error
            }
        }

        div(classes = "form-group") {
            customSelectWithLabel("Island", user.islands.map { it.id.value.toString() to it.name }, inputName = "island", prefill = islandPrefill) {
                append {
                    aBtn(BtnType.SUCCESS, classes = "text-light", href = call.locations.href(IslandsLocation.New())) { +"New" }
                }
            }
        }

        div(classes = "form-group") {
            customSelectWithLabel("Time", EntryStamp.values().map { it.ordinal.toString() to it.displayName }, inputName = "entryStamp", prefill = entryStampPrefill.toString())
        }

        div(classes = "form-group") {
            inputGroupWithLabel("Price", inputName = "price", type = InputType.number, prefill = pricePrefill.toString()) {
                append {
                    btn(BtnType.SUCCESS, type = ButtonType.submit) { +"Save" }
                }
            }
        }
    }
}
