package views.islands

import controllers.index.IndexController
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import kotlinx.html.*
import model.Island
import views.respondTurnipPage
import views.util.bTableHeader
import views.util.bTableTR
import views.util.makeRows

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondIslandListPage(islands: Iterable<Island>) = respondTurnipPage("Islands") {

    h3 { +"Islands" }
    small(classes = "text-muted") { +"Showing islands with entries within 7 days" }
    hr {}

    bTableTR(striped = true, hover = true, sm = true) {
        bTableHeader("Island", "Owner", "Last Entry", dark = true)
        makeRows(islands.sortedByDescending { it.lastEntryStamp }) { island ->
            td {
                a(island.href(this@respondIslandListPage)) {
                    +island.name
                }
            }
            td { +(island.owner?.name ?: "Unclaimed") }
            td { +(island.lastEntry?.entryStampEnum?.displayName ?: "None") }
        }
    }

}
