package views.islands

import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import kotlinx.html.*
import model.Island
import views.respondTurnipPage
import views.util.*

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondIslandClaimPage(island: Island, incorrectPassword: Boolean = false) {
    val user = getUser()!!

    respondTurnipPage(island.name) {

        h3 { +"Claiming ${island.name} for ${user.name}" }
        hr {}

        div(classes = "alert alert-danger") {
            strong { +"Claiming an island is irreversible!" }
            br {}
            +"Only claim an island if it truly belongs to you."
        }

        postForm {
            if (incorrectPassword)
                small(classes = "text-danger") { +"Incorrect password." }

            inputGroupWithLabel("Password (if required)", inputName = "password", type = InputType.password) {
                append {
                    btn(BtnType.SUCCESS, type = ButtonType.submit) { +"Claim" }
                }
            }
        }

    }
}
