package views.islands

import controllers.friends.FriendsLocation
import controllers.islands.IslandsLocation
import features.getUser
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import model.*
import org.jetbrains.exposed.sql.and
import views.respondTurnipPage
import views.util.ResponsiveBreakpoint
import views.util.col
import views.util.row

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondIslandPage(island: Island) {
    val currentUser = getUser()

    respondTurnipPage(island.name) {
        val allEntries = island.entries.toList()

        println(island.startOfWeek)
        val entries = EntryStamp.values().map { time ->
            time to allEntries.lastOrNull { it.week.isEqual(island.startOfWeek) && it.entryStampEnum == time }
        }.toMap()

        h3(classes = "pt-4") { +island.name }
        hr {}

        val owner = island.owner
        if (owner != null) {
            br {}
            span("text-muted") { +"Owner: ${owner.name} " }

            // region friend requests
            if (currentUser != null && owner.id.value != currentUser.id.value) {
                val isFriend = !Friend.find { (Friends.owner eq currentUser.id) and (Friends.target eq owner.id) }.empty()
                val sentReq = !isFriend && !FriendRequest.find { (FriendRequests.sender eq currentUser.id) and (FriendRequests.receiver eq owner.id) }.empty()
                val receivedReq = !sentReq && !FriendRequest.find { (FriendRequests.sender eq owner.id) and (FriendRequests.receiver eq currentUser.id) }.empty()
                when {
                    isFriend || sentReq -> {
                        br {}
                        button(classes = "btn btn-outline-secondary btn-sm disabled") {
                            disabled = true
                            +(if (isFriend) "Already Friends" else "Sent Friend Request")
                        }
                    }
                    else -> postForm(action = locations.href(FriendsLocation.Requests())) {
                        id = "add-friend-form"
                        hiddenInput(name = "snowflake") { value = owner.snowflake }
                        hiddenInput(name = "redirect-island") { value = island.id.value.toString() }
                        button(type = ButtonType.submit, classes = "btn btn-outline-secondary btn-sm", name = "action") {
                            value = "add"
                            +(if (receivedReq) "Accept Friend Request" else "Send Friend Request")
                        }
                    }
                }
            }
            // endregion
        } else {
            a(href = locations.href(IslandsLocation.Identified.Claim(island.id.value))) { +"Claim this island!" }
        }


        row {

            col(ResponsiveBreakpoint.MD, flexBoxWidth = 8) {
                div(classes = "chart-container") {
                    style = "height: 100%; min-height: 40vh; width: 100%"
                    canvas {
                        id = "island-canvas-root"
                    }
                }
            }

            col(ResponsiveBreakpoint.MD) {
                div(classes = "table-responsive") {
                    table(classes = "table table-striped table-sm table-hover") {
                        thead {
                            tr {
                                th(scope = ThScope.col) { +"Time" }
                                th(scope = ThScope.col) { +"Price" }
                            }
                        }
                        tbody {
                            var c = 0
                            for (time in EntryStamp.values()) {
                                tr {
                                    td { +time.displayName }
                                    td {
                                        id = "turnip-price-" + c++.toString() // used in the chart js
                                        +(entries[time]?.price?.toString() ?: "")
                                    }
                                }
                            }
                        }
                    }
                }
                div(classes = "float-right") {
                    a(classes = "ml-auto", href = locations.href(IslandsLocation.Identified.Redirect(island.id.value))) { +"Detailed Predictions" }
                }
            }

        }

        script(src = "/static/scripts/ext/Chart.bundle.min.js") {}
        script(src = "/static/scripts/ext/uint32.js") {}
        script(src = "/static/scripts/ext/uint64.js") {}
        script(src = "/static/scripts/ext/lodash.min.js") {}
        script(src = "/static/scripts/ext/moment.min.js") {}
        script(src = "/static/scripts/calc/optimizer.js") {}
        script(src = "/static/scripts/calc/calculator.js") {}
        script(src = "/static/scripts/island-canvas.js") { defer = true }
    }
}
