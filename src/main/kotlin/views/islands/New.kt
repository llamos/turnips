package views.islands

import controllers.index.IndexController
import controllers.islands.IslandsLocation
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import model.Island
import org.joda.time.DateTimeZone
import views.respondTurnipPage
import views.util.*
import kotlin.text.append

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondIslandCreatePage(error: String? = null) = respondTurnipPage("New Island") {

    val availableTimezones = DateTimeZone.getAvailableIDs()
        .map { it to it }

    h3 { +"New Island" }
    hr {}
    div("alert alert-warning") {
        +"If you instead want to claim an existing island, check the "
        a(href = locations.href(IslandsLocation())) { +"island list" }
        +" first and claim your island from there. "
        +"If you have submitted before but not in the past week, "
        +"please contact Lemon."
    }

    if (error != null)
        small("text-danger") { +error }
    postForm {
        customSelectWithLabel("Time Zone", inputName = "timezone", options = availableTimezones, prefill = "America/Toronto")
        inputGroupWithLabel("Island Name", type = InputType.text, inputName = "name") {
            append {
                btn(BtnType.SUCCESS, type = ButtonType.submit) { +"Save" }
            }
        }
    }

}
