package views

import controllers.auth.AuthLocation
import controllers.friends.FriendsLocation
import controllers.index.IndexLocation
import controllers.islands.IslandsLocation
import io.ktor.application.ApplicationCall
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import views.util.BtnType
import views.util.btn
import views.util.navItem
import model.User

private fun DIV.authButton(href: String, src: String, body: BUTTON.() -> Unit) {
    a(href = href, classes = "ml-sm-2") {
        btn(btnType = BtnType.LIGHT, classes = "my-2 my-sm-0") {
            img(src="/static/images/${src}", classes = "mr-1") {
                height = "24sp"
                width = "auto"
            }
            body()
        }
    }
}

@KtorExperimentalLocationsAPI
fun BODY.turnipNavbar(user: User?, call: ApplicationCall) {
    nav(classes = "navbar navbar-expand-lg navbar-dark bg-dark mb-4") {
        // toggle button
        a(classes = "navbar-brand", href = call.locations.href(IndexLocation())) { +"Turnip Tracker" }
        button(classes = "navbar-toggler", type = ButtonType.button) {
            attributes["data-toggle"] = "collapse"
            attributes["data-target"] = "#navbarContent"
            span(classes = "navbar-toggler-icon") {}
        }

        // menu items
        div(classes = "collapse navbar-collapse") {
            id = "navbarContent"
            ul(classes = "navbar-nav mr-auto") {
                navItem(href = call.locations.href(IndexLocation()), active = true) { +"Home" }
                navItem(href = call.locations.href(IslandsLocation())) { +"Islands" }
            }

            // user sign-in btn/menu
            if (user != null) {
                // user menu
                div(classes = "dropdown") {
                    btn(btnType = BtnType.LIGHT, classes = "dropdown-toggle", type = ButtonType.button) {
                        id = "navbarUserDropdownButton"
                        attributes["data-toggle"] = "dropdown"
                        +user.name
                    }
                    div(classes="dropdown-menu") {
                        a(classes = "dropdown-item", href = call.locations.href(FriendsLocation())) { +"My Friends" }
                        div(classes = "dropdown-divider") {}
                        a(classes = "dropdown-item", href = call.locations.href(AuthLocation.SignOutLocation())) { +"Sign Out" }
                    }
                }
            } else {
                // sign-in buttons
                authButton(href = call.locations.href(AuthLocation.SignInLocation.Discord()), src = "discord_logo.png") { +"Sign in with Discord" }
            }
        }
    }
}
