package views.error

import controllers.auth.AuthLocation
import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.locations
import kotlinx.html.*
import views.respondTurnipPage

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondUnauthenticated() = respondTurnipPage("Unauthenticated", HttpStatusCode.Unauthorized) {
    h3 { +"Unauthenticated" }
    hr {}
    p {
        +"You attempted to access a protected page while not signed in. Please "
        a(href = locations.href(AuthLocation.SignInLocation.Discord())) { +"sign in" }
        +" and try again."
    }
}
