package views.error

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.request.httpMethod
import io.ktor.request.path
import io.ktor.request.receiveText
import io.ktor.util.KtorExperimentalAPI
import io.ktor.util.toMap
import views.util.card
import views.util.cardBody
import kotlinx.html.h3
import kotlinx.html.hr
import kotlinx.html.p
import kotlinx.html.pre
import views.respondTurnipPage

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
suspend fun ApplicationCall.respondInternalServerError(e: Exception) {
    val body = receiveText()
    respondTurnipPage("Internal Server Error", HttpStatusCode.InternalServerError) {
        h3 { +"Error" }
        hr {}
        p { +"An unexpected error has occurred. Please report this to Lemon." }
        card {
            cardBody {
                pre {
                    +generatePreformatted(body)
                    +e.generatePreformatted()
                }
            }
        }
    }
}

private fun String.sanitize(): String {
    return replace("<", "&lt;").replace(">", "&gt;")
}

@KtorExperimentalAPI
private fun ApplicationCall.generatePreformatted(body: String): String {
    val req = request
    return buildString {
        appendln("==Path==")
        append(req.httpMethod.value.sanitize())
        append(" ")
        appendln(req.path().sanitize())
        appendln()

        appendln("==Headers==")
        for (header in req.headers.toMap()) {
            if (header.key == "Cookie")
                continue

            append(header.key.sanitize())
            append("=")
            appendln(header.value.joinToString(separator = ",") { it.sanitize() })
        }
        appendln()

        appendln("==Cookies==")
        for (cookie in req.cookies.rawCookies) {
            append(cookie.key.sanitize())
            append("=")
            appendln(cookie.value.sanitize())
        }
        appendln()

        if (!req.queryParameters.isEmpty()) {
            appendln("==Query Parameters==")
            for (param in req.queryParameters.toMap()) {
                append(param.key.sanitize())
                append("=")
                appendln(param.value.joinToString(separator = ",") { it.sanitize() })
            }
            appendln()
        }

        if (body.isNotBlank()) {
            appendln("==Body==")
            append(body.sanitize())
            appendln()
        }

    }
}

private fun Exception.generatePreformatted(): String {
    val e = this
    return buildString {
        appendln("==Stack==")

        // basic exception info
        append(e.javaClass.canonicalName.sanitize())
        if (e.message != null) {
            append(": ")
            append(e.message!!.sanitize())
        }
        appendln()

        // stack traversal
        var skipCount = 0
        for (entry in e.stackTrace) {
            if (entry.isProgramLocal()) {
                // display skipped lines
                if (skipCount > 0) {
                    append("    ... (skipped $skipCount calls)\n")
                    skipCount = 0
                }

                append("    at ${entry.className}#${entry.methodName}")
                if (entry.fileName != null) {
                    append(" (${entry.fileName}")
                    if (entry.lineNumber >= 0)
                        append(":${entry.lineNumber}")
                    append(")")
                }
                appendln()
            } else {
                skipCount++
            }
        }

        // display skipped lines
        if (skipCount > 0) {
            append("    ... (skipped $skipCount calls)\n")
        }
    }
}

private val programPackages = listOf(
    "controllers",
    "features",
    "forms",
    "model",
    "views"
)
private fun StackTraceElement.isProgramLocal(): Boolean =
    programPackages.contains(className.split(".")[0])
