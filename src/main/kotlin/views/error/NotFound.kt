package views.error

import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import kotlinx.html.h3
import kotlinx.html.hr
import kotlinx.html.p
import views.respondTurnipPage

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondNotFound() = respondTurnipPage("Not Found", HttpStatusCode.NotFound) {
    h3 { +"Not Found" }
    hr {}
    p { +"The page you requested was not found. Please go back and try again." }
}
