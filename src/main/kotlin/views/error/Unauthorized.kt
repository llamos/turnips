package views.error

import io.ktor.application.ApplicationCall
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import kotlinx.html.h3
import kotlinx.html.hr
import kotlinx.html.p
import views.respondTurnipPage

@KtorExperimentalLocationsAPI
suspend fun ApplicationCall.respondUnauthorized() = respondTurnipPage("Unauthorized", HttpStatusCode.Unauthorized) {
    h3 { +"Unauthorized" }
    hr {}
    p {
        +"You attempted to access a protected page but do not have permission to view this. "
        +"If you believe this is in error, please contact Lemon."
    }
}
