import com.zaxxer.hikari.HikariDataSource
import controllers.Controller
import controllers.auth.AuthController
import controllers.error.ExceptionResponder
import controllers.error.UnauthenticatedResponder
import controllers.error.UnauthorizedResponder
import controllers.error.UnsupportedOperationResponder
import controllers.friends.FriendsController
import controllers.index.IndexController
import controllers.islands.IslandsController
import features.DiscordAuth
import features.TurnipAuth
import io.ktor.application.Application
import io.ktor.application.ApplicationCallPipeline
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.*
import io.ktor.http.content.resources
import io.ktor.http.content.static
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.request.path
import io.ktor.routing.routing
import io.ktor.util.KtorExperimentalAPI
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database
import org.slf4j.event.Level
import views.error.respondNotFound

@KtorExperimentalLocationsAPI
@KtorExperimentalAPI
@kotlin.jvm.JvmOverloads
@Suppress("UNUSED", "UNUSED_PARAMETER") // Referenced in application.conf
fun Application.module(testing: Boolean = false) {

    val source = HikariDataSource().apply {
        jdbcUrl = environment.config.property("db.url").getString()
        username = environment.config.property("db.user").getString()
        password = environment.config.property("db.pass").getString()
        isAutoCommit = false
    }

    Flyway.configure()
        .dataSource(source)
        .locations("classpath:migrations")
        .load()
        .migrate()

    Database.connect(source)

    install(Locations)
    install(TurnipAuth)
    install(DiscordAuth) {
        clientId = environment.config.property("discord.clientId").getString()
        clientSecret = environment.config.property("discord.clientSecret").getString()
    }

    install(StatusPages) {
        exception(UnsupportedOperationResponder)
        exception(UnauthenticatedResponder)
        exception(UnauthorizedResponder)
        exception(ExceptionResponder)
    }

    install(Compression) {
        gzip {
            priority = 1.0
        }
        deflate {
            priority = 10.0
            minimumSize(1024) // condition
        }
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(DoubleReceive) {
        receiveEntireContent = true
    }

    install(ForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy
    install(XForwardedHeaderSupport) // WARNING: for security, do not include this if not behind a reverse proxy

    val controllers = listOf<Controller>(
        AuthController,
        FriendsController,
        IndexController,
        IslandsController
    )

    routing {
        for (c in controllers)
            c.route(this)

        // Static feature. Try to access `/static/ktor_logo.svg`
        static("/static") {
            resources("static")
        }
    }

    intercept(ApplicationCallPipeline.Fallback) {
        if (call.response.status() == null)
            call.respondNotFound()
    }
}

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
