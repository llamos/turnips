function populate(prev) {
    let dataSet = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map(v => parseInt(document.getElementById(`${prev ? 'prev-' : ''}turnip-price-${v}`).innerText) || undefined);

    let patterns = possiblePatterns(dataSet);
    let patternCount = patterns.reduce((acc, cur) => acc + cur.length, 0);
    if (patternCount === 0) patterns = possiblePatterns([0, ...dataSet.slice(1)]);
    let minMaxPattern = patternReducer(patterns);
    let minMaxData = _.zip(...minMaxPattern);
    let avgPattern = patternReducer(patterns, averageReducer);
    let avgData = _.zip(...avgPattern);
    let [minWeekValue] = patternReducer(patterns, minWeekReducer);

    let minLine = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        .map(i => dataSet[i+1] || (minMaxData[0] ? minMaxData[0][i] : NaN));
    let maxLine = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        .map(i => dataSet[i+1] || (minMaxData[1] ? minMaxData[1][i] : NaN));
    let avgLine = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        .map(i => dataSet[i+1] || (avgData[0] ? Math.trunc(avgData[0][i]) : NaN));

    [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].forEach(v => {
        if (!isFinite(parseInt(document.getElementById(`${prev ? 'prev-' : ''}turnip-price-${v}`).innerText))) {
            document.getElementById(`${prev ? 'prev-' : ''}turnip-price-${v}`).innerHTML = `<span class="text-muted">${minMaxData[0] ? minMaxData[0][v - 1] : '???'}-${minMaxData[1] ? minMaxData[1][v - 1] : '???'}</span>`;
        }
    });

    let config = {
        type: 'line',
        data: {
            labels: ['Mon AM', 'Mon PM', 'Tue AM', 'Tue PM', 'Wed AM', 'Wed PM', 'Thu AM', 'Thu PM', 'Fri AM', 'Fri PM', 'Sat AM', 'Sat PM'],
            datasets: [
                {
                    label: 'Buy Price',
                    backgroundColor: 'transparent',
                    borderColor: '#273e47',
                    data: new Array(12).fill(dataSet[0] || NaN),
                    fill: true,
                    pointRadius: 0,
                    pointHoverRadius: 0,
                    borderDash: [5, 15]
                },
                {
                    label: 'Guaranteed Minimum',
                    data: new Array(12).fill(minWeekValue || NaN),
                    fill: true,
                    backgroundColor: "transparent",
                    borderColor: '#007D75',
                    pointRadius: 0,
                    pointHoverRadius: 0,
                    borderDash: [3, 6]
                },
                {
                    label: 'Daily Price',
                    data: Array.from({length: 12}, (v, i) => dataSet[i + 1] || NaN),
                    fill: false,
                    backgroundColor: '#a4243b',
                    borderColor: '#a4243b'
                },
                {
                    label: 'Average',
                    data: avgLine,
                    backgroundColor: '#b49286',
                    borderColor: '#b49286',
                    pointRadius: 0,
                    fill: false
                },
                {
                    label: 'Maximum',
                    data: maxLine,
                    backgroundColor: '#c2eabd',
                    borderColor: '#d8973c',
                    pointRadius: 0,
                    pointHoverRadius: 0,
                    fill: 3
                },
                {
                    label: 'Minimum',
                    data: minLine,
                    backgroundColor: '#dcf2b0',
                    borderColor: '#dcf2b0',
                    pointRadius: 0,
                    pointHoverRadius: 0,
                    fill: 3
                }
            ]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            title: {
                display: true,
                text: `Prices ${prev ? 'Last' : 'This'} Week`
            },
            tooltips: {
                mode: 'index',
                intersect: false,
            },
            hover: {
                mode: 'nearest',
                intersect: true
            },
            scales: {
                yAxes: [
                    {
                        gridLines: {
                            display: false
                        },
                        ticks: {
                            suggestedMin: 0,
                            suggestedMax: 300
                        }
                    }
                ]
            },
        }
    };

    new Chart(`${prev ? 'prev-' : ''}island-canvas-root`, config);
}

populate(false);
populate(true);
