CREATE TABLE entries (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    entry_stamp DATETIME NOT NULL,
    island_name VARCHAR(20) NOT NULL,
    price INT NOT NULL
);
