create table friends (
    id int not null auto_increment primary key,
    owner int not null,
    target int not null,
    foreign key fk_friends_owner (owner) references users (id),
    foreign key fk_friends_target (target) references users (id)
);

create table friend_requests (
    id int not null auto_increment primary key,
    sender int not null,
    receiver int not null,
    entry_stamp timestamp not null,
    foreign key fk_friend_requests_sender (sender) references users (id),
    foreign key fk_friend_requests_receiver (receiver) references users (id)
);
