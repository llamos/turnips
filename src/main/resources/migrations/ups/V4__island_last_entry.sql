alter table islands
    add column last_entry datetime null default null;

update islands i
set last_entry=(select max(entry_stamp) from entries where island = i.id);
