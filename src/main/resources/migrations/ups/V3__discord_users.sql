create table users (
    id int not null auto_increment primary key,
    discord_snowflake varchar(18) not null,
    name varchar(40) not null
);

alter table islands
    add column owner int null,
    add foreign key fk_islands_owner (owner) references users (id);
