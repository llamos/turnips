alter table islands
    add column timezone varchar(60) not null default 'America/Toronto';
