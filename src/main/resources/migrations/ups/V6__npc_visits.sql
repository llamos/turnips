create table npc_visits (
    id int not null auto_increment primary key,
    island int not null,
    entry_stamp datetime not null,
    npc int not null,
    foreign key fk_npc_visits_island (island) references islands (id)
);
