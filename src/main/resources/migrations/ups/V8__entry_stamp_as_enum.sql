alter table entries
    add column week             datetime null after id,
    add column entry_stamp_enum int      null after week;

# noinspection SqlWithoutWhere
update entries,islands
set week=convert_tz(subdate(date(entry_stamp), dayofweek(date(entry_stamp)) - 1), islands.timezone, @@global.time_zone),
    entry_stamp_enum=(
        if((dayofweek(entry_stamp) - 1) = 0,
           0,
           ((dayofweek(entry_stamp) - 1) * 2) - if(hour(entry_stamp) < 16, 1, 0))
        )
where entries.island = islands.id;

alter table entries
    modify column week datetime not null,
    modify column entry_stamp_enum int not null;

alter table islands
    rename column last_entry to last_entry_stamp;

alter table npc_visits
    add column week             datetime null after id,
    add column entry_stamp_enum int      null after week;

# noinspection SqlWithoutWhere
update npc_visits,islands
set week=convert_tz(subdate(date(entry_stamp), dayofweek(date(entry_stamp)) - 1), islands.timezone, @@global.time_zone),
    entry_stamp_enum=(
        if((dayofweek(entry_stamp) - 1) = 0,
           0,
           ((dayofweek(entry_stamp) - 1) * 2) - 1)
        )
where npc_visits.island = islands.id;

alter table npc_visits
    modify column week datetime not null,
    modify column entry_stamp_enum int not null,
    drop column entry_stamp;

