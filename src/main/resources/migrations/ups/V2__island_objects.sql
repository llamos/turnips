CREATE TABLE islands (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    island_name VARCHAR(20) NOT NULL,
    password_hash text NULL
);

INSERT INTO islands (island_name)
SELECT DISTINCT(island_name) FROM entries;

ALTER TABLE entries
    ADD COLUMN island INT NOT NULL;

UPDATE entries e
    SET e.island=(SELECT i.id FROM islands i WHERE i.island_name=e.island_name);

ALTER TABLE entries
    ADD FOREIGN KEY fk_entries_island (island) REFERENCES islands (id),
    DROP COLUMN island_name;

